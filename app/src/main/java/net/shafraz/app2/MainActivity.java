package net.shafraz.app2;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import net.shafraz.app1.IWord;

public class MainActivity extends AppCompatActivity {

    static final int PICK_FILE_CODE = 123;
    static final int READ_FILE = 0;
    static final int FILE_DID_READ = 1;
    static final int SHOW_RESULTS = 2;

    private static Handler mHandler;
    private Thread mFileReaderThread;
    private Thread mProcessThread;

    EditText mSource ;
    EditText mResult ;

    private IWord service = null;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            service = IWord.Stub.asInterface(iBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            service = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSource = (EditText)findViewById(R.id.source);
        mResult = (EditText)findViewById(R.id.result);

        mHandler = new Handler(){
            @Override
            public void handleMessage(Message message){
                switch (message.what) {
                    case READ_FILE:
                        mFileReaderThread = new FileReaderThread((String)message.obj);
                        mFileReaderThread.start();
                        break;
                    case FILE_DID_READ:
                        mFileReaderThread = null;
                        mSource.setText((String)message.obj);
                        mProcessThread = new ProcessThread((String)message.obj);
                        mProcessThread.start();
                        break;
                    case SHOW_RESULTS:
                        mResult.setText((String)message.obj);
                        mProcessThread = null;
                        break;
                }
            }
        };

        bindService(new Intent("net.shafraz.WordService"),connection,Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(connection);
    }


    public void loadFile(View view){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("text/plain");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    PICK_FILE_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;

        switch (requestCode) {
            case PICK_FILE_CODE:
                if (resultCode == RESULT_OK) {
                    String filePath = getPath(this,data.getData());
                    Message message = mHandler.obtainMessage();
                    message.what = READ_FILE;
                    message.obj = filePath;
                    mHandler.sendMessage(message);
                }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getPath(final Context context, final Uri uri) {
        if(DocumentsContract.isDocumentUri(context, uri)) {

            final String docId = DocumentsContract.getDocumentId(uri);

            if (isExternalStorageDocument(uri)) {

                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                } else {
                    String path = Environment.getExternalStoragePublicDirectory(type).toString().replaceAll(type,"");
                    return path + split[1];
                }
            }
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            else if (isMediaDocument(uri)) {
                return "Media " + docId;
            }
            return null;
        }

        return null;
    }


    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    class FileReaderThread extends Thread {
        String mFilePath;

        public FileReaderThread(String filePath) {
            super();
            mFilePath = filePath;
        }

        @Override
        public void run() {
            StringBuilder text = new StringBuilder();

            File file = new File(mFilePath);
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
            }
            catch (IOException e) {
                Log.e("File Error",e.toString());
            }

            Message message = mHandler.obtainMessage();
            message.what = FILE_DID_READ;
            message.obj = text.toString();
            mHandler.sendMessage(message);
        }
    }

    class ProcessThread extends Thread {
        String mString;

        public ProcessThread(String string) {
            super();
            mString = string;
        }

        @Override
        public void run() {
            super.run();
            try {
                String result = service.process(mString);
                Message message = mHandler.obtainMessage();
                message.what = SHOW_RESULTS;
                message.obj = result;
                mHandler.sendMessage(message);
            } catch (RemoteException ex) {mResult.setText(ex.toString());}
        }
    }
}
